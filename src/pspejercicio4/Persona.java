package pspejercicio4;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 * * Represents a person.
 *
 *
 * @author Eugenia Pérez Martínez
 */
class Persona {

    private int age;
    private String dni;

    //TODO
    /**
     * Class constructor.
     */

    public Persona() {
        //Initialize attributes to random values.The age must be an integer 
        //between 0 and 100, and the dni a 8 characters alphanumeric string 

        this.age = (int) Math.floor(Math.random() * (100 + 1));

        String juegoCaracteres = "TRWAGMYFPDXBNJZSQVHLCKE";

        int numeroDni = (int) Math.floor(Math.random() * (99999999 + 1));
        int modulo = numeroDni % 23;
        char letra = juegoCaracteres.charAt(modulo);

        this.dni = numeroDni + "-" + letra;
    }

    @Override
    public String toString() {
        return "Persona{" + "age=" + age + ", dni=" + dni + '}';
    }

}
