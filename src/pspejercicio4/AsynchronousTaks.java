package pspejercicio4;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
/**
 * * Represents a task that inserts a number of people in a list.
 *
 * *
 * @author Eugenia Pérez Martínez
 */
public class AsynchronousTaks {

    /**
     * * Entry point of the app. * * @param args the command line arguments
     */

    public static void main(String[] args) throws InterruptedException {

        int numberOfPeople = 20;
        Thread t1 = new Thread(new PushPeopleToFile(numberOfPeople));
        t1.start();
        t1.join();
        Thread t2 = new Thread(new PushPeopleToList(numberOfPeople));
        t2.start();

    }
}
