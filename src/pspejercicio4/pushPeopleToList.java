package pspejercicio4;

/**
 *
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList; 
import java.util.List; 
import java.util.logging.Level; 
import java.util.logging.Logger; 

/** * Represents a task that inserts a number of people in a list. * 
 * @author Eugenia Pérez Martínez */ 
class PushPeopleToList implements Runnable { 
    
    /** * Number of people to insert. */ 
    private int numberOfPeople;

    /** * Class constructor. * 
     * @param numberOfPeople people to insert. */ 
    public PushPeopleToList(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public void run() {
        System.out.println("Starting to push people to an ArrayList");

        ArrayList<Persona> list = new ArrayList<Persona>();
        Persona p;

        for (int i = 0; i < this.numberOfPeople; i++) {

            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(PushPeopleToList.class.getName()).log(Level.SEVERE, null, ex);
            }

            p = new Persona();
            list.add(p);
            System.out.println("Se ha introducido en la lista la persona: " + p.toString());

        }

        //TODO: Crea un ArrayList. A continuación, dentro de un bucle con el 
        //mismo número de iteraciones que personas a insertar, crea una espera 
        //de medio segundo, crea la persona a insertar e insértala 
        System.out.println("Everyone is already in the ArrayList");
    }
}
