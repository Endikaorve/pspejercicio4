package pspejercicio4;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * * Represents a task that inserts a number of people in a text file.
 *
 *
 * @author Eugenia Pérez Martínez
 */
class PushPeopleToFile implements Runnable {

    /**
     * * Number of people to insert.
     */
    private int numberOfPeople;

    /**
     * * Class constructor.
     *
     *
     * @param numberOfPeople people to insert.
     */
    public PushPeopleToFile(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public void run() {
        System.out.println("Starting to push people to a text file");
        //TODO: Crea los objetos necesarios para escribir a fichero en Java. 

        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("personas.txt");
            pw = new PrintWriter(fichero);

            for (int i = 0; i < this.numberOfPeople; i++) {
                pw.println(new Persona().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        System.out.println("Everyone is already in a text file");
    }
}
